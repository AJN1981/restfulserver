﻿using Rados.OreSorter.Domain.Models;
using Spire.Xls;
using Spire.Xls.Converter;
using System.IO;

namespace Rados.OreSorter.Services.XlsxToPdf
{
	public class XlsxToPdfService : IXlsxToPdfService
	{
		public FileModel GeneratePDF(FileModel inputfile)
		{
			// Result Stream
			MemoryStream pdfMemoryStream = new MemoryStream();
			Workbook workbook = new Workbook();
			workbook.Version = ExcelVersion.Version2013;
			// Spire.XLS to open XLSX workbook stream created by EPPlus
			using (MemoryStream xlsxMemoryStream = new MemoryStream(inputfile.Content))
			{ 
			workbook.LoadFromStream(xlsxMemoryStream,ExcelVersion.Version2013);
			}
			// Spire.PDF to convert XLSX to PDF, I read it has limited functionality (total pages, rows, etc...).			
			PdfConverterSettings settings = new PdfConverterSettings();
			workbook.SaveToStream(pdfMemoryStream, FileFormat.PDF);

			//inputfile.			
			return new FileModel
			{

				FileName = Path.ChangeExtension(inputfile.FileName, ".pdf" ),
				ContentType = "application/pdf",
				Content = pdfMemoryStream.ToArray()
			};
		}
	}
}
