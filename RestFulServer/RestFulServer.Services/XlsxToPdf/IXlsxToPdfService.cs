﻿using Rados.OreSorter.Domain.Models;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Rados.OreSorter.Services.XlsxToPdf
{
	public interface IXlsxToPdfService
	{
		FileModel GeneratePDF(FileModel inputfile);
	}
}