﻿using FileServer.Services.Files;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using WinSCP;


namespace FileServer.Services.sFTP
{
    public class sFTP : IsFTP
    {
        public string filename
        {
            get
            {
                string dataPath = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "Settings");
                Directory.CreateDirectory(dataPath);
                return Path.Combine(dataPath, "FileSettings.json");
            }
            set {; }
        }
        private static FileSettings _settings;
        public FileSettings Settings
        {
            get
            {
                _settings = new FileSettings();
                _settings.Load(filename);
                return _settings;
            }
        }



        public SessionOptions sessionOptions = new SessionOptions
        {
            Protocol = Protocol.Sftp,
            HostName = "192.168.1.2",
            UserName = "mass",
            Password = "mass",
            SshHostKeyPolicy = SshHostKeyPolicy.GiveUpSecurityAndAcceptAny
        };
        public TransferOptions transferOptions = new TransferOptions
        {
            OverwriteMode = OverwriteMode.Append
        };
        public string MSC3_Path = "/mass/Tests";
        public string localPath = @"C:\Test";

        public List<string> GetFilesOnRemote()
        {
            try
            {
                using (Session session = new Session())
                {
                    List<string> _files = new List<string>();
                    _files.Add("Something");
                    // Connect 

                    session.Open(sessionOptions);
                    var dirList = session.ListDirectory(MSC3_Path);
                    _files = dirList.Files.Where(x => !x.IsDirectory).Select(x => x.Name).ToList();
                    return _files;
                }
            }
            catch (Exception e)
            {
                List<string> _files = new List<string>();
                _files.Add(e.ToString());
                return _files;
            }
        }
        public List<string> CopyAllFromRemote()
        {
            try
            {
                using (Session session = new Session())
                {
                    List<string> _files = new List<string>();
                    session.Open(sessionOptions);
                    foreach (var FileType in Settings.fileFolders)
                    {
                        Directory.CreateDirectory(FileType.Dir);
                        var dirList = session.GetFilesToDirectory(MSC3_Path, FileType.Dir, "*" + FileType.FileExt, false, transferOptions);
                        _files.AddRange(dirList.Transfers.Select(x => x.FileName).ToList());
                    }
                    return _files;
                }
            }
            catch (Exception e)
            {
                List<string> _files = new List<string>();
                _files.Add(e.ToString());
                return _files;
            }
        }

        public List<string> DeleteOld()
        {
            try
            {
                using (Session session = new Session())
                {
                    List<string> _files = new List<string>();
                    session.Open(sessionOptions);
                    var dirList = session.ListDirectory(MSC3_Path);
                    var tmp = dirList.Files.Where(x => (x.LastWriteTime <= DateTime.Now.AddDays(-1 * Math.Abs(Settings.DaysKeep))) & (!x.IsDirectory));
                    foreach (var _file in dirList.Files.Where(x => (x.LastWriteTime <= DateTime.Now.AddDays(-1 * Math.Abs(Settings.DaysKeep)))&(!x.IsDirectory)))
                    {
                        session.RemoveFile(_file.FullName);
                        _files.Add(_file.FullName + " " + _file.LastWriteTime);
                    }
                    return _files;
                }
            }
            catch (Exception e)
            {
                List<string> _files = new List<string>();
                _files.Add(e.ToString());
                return _files;
            }
        }

    }
}
