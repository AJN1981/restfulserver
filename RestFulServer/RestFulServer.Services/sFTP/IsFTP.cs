﻿using System;
using System.Collections.Generic;
using System.Text;

namespace FileServer.Services.sFTP
{
    public interface IsFTP
    {
        List<string> GetFilesOnRemote();
        List<string> CopyAllFromRemote();
        List<string> DeleteOld();
    }
}
