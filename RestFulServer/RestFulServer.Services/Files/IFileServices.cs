﻿using System;
using System.Collections.Generic;
using System.Text;

namespace FileServer.Services.Files
{
	public interface IFileServices
	{
		List<string> FileNames();
		void WriteFileNames(List<string> fileNames);

		void RecieveFile(string FileName, string data);
		void RecieveFileNames(List<string> FileNames);
		string FileNameRequest();
		List<string> FilesReadyForDelete();

		FileSettings GetSettings();
		void PostFileFolder(FileFolders value);

	}
}
