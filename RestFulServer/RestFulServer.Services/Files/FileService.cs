﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace FileServer.Services.Files
{
	public class FileService : IFileServices
	{
		//private static string dir = @"C:\TestFolder\";
		private static int count = 1;
		//private static int DaysKeep = 5; 
		private static List<string> _filesNamesOnMSC;


		public string filename
		{
			get
			{
				string dataPath = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "Settings");
				Directory.CreateDirectory(dataPath);
				return Path.Combine(dataPath, "FileSettings.json");
			}
			set {; }
		}
		private static FileSettings _settings;
		public FileSettings Settings
		{
			get
			{
				if (_settings == null)
				{
					_settings = new FileSettings();
					_settings.Load(filename);
				}
				return _settings;
			}
		}
		public FileSettings GetSettings()
		{
			return Settings;
		}
		public void PostFileFolder(FileFolders value)
		{
			if (_settings.fileFolders ==null)
			{
				_settings.fileFolders = new List<FileFolders>();
			}
			_settings.fileFolders.Add(value);
			_settings.Save(filename);
		}


		public List<string> FileNames()
		{
			if (_filesNamesOnMSC == null)
			{
				_filesNamesOnMSC = new List<string>();
				_filesNamesOnMSC.Add("Empty");
			}
			//var tmp =_cloud.postFile();
			return _filesNamesOnMSC;

		}

		public void WriteFileNames(List<string> fileNames)
		{
			if (fileNames != null)
			{
				_filesNamesOnMSC = fileNames;
			}
		}

		public void RecieveFile(string FileName, string data)
		{
			var FilePath = getFilePath(FileName);
			if (!File.Exists(FilePath))
			{
				using (StreamWriter file = new StreamWriter(FilePath))
				{
					file.Write(data);
				}
			}

		}
		private string getFilePath(string FileName)
		{
			string _dir = Settings.dir;
			string fileType = Path.GetExtension(FileName);
			var temp = Settings.fileFolders.Where(i => i.FileExt == fileType).FirstOrDefault();
			if (temp != null)
			{
				_dir = temp.Dir;
			}
			Directory.CreateDirectory(_dir);
			//switch (fileType)
			//{
			//	case ".csv":
			//		_dir += @"Test\";
			//		break;
			//	default:
			//		_dir += @"Settings\";
			//		break;

			//}
			return Path.Combine(_dir, FileName);
		}


		public void RecieveFileNames(List<string> FileNames)
		{
			if (FileNames != null)
			{
				_filesNamesOnMSC = FileNames;
			}
		}

		public string FileNameRequest()
		{
			if (_filesNamesOnMSC != null)
			{
				foreach (string filename in _filesNamesOnMSC)
				{

					if (!File.Exists(getFilePath(filename)))
					{ return filename; }
				}
			}
			return null;
		}
		public List<string> FilesReadyForDelete()
		{
			List<string> Files = new List<string>();
			if (_filesNamesOnMSC != null)
			{
				foreach (string filename in _filesNamesOnMSC)
				{
					var FilePath = getFilePath(filename);
					if (File.Exists(FilePath))
					{
						if (File.GetCreationTime(FilePath).AddDays(Settings.DaysKeep) > DateTime.Now)
						{
							Files.Add(filename);
						}
					}
				}
			}
			return Files;
		}


	}
}
