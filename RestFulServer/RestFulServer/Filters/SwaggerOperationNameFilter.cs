﻿
using Microsoft.AspNetCore.Mvc.Controllers;
using Swashbuckle.AspNetCore.Swagger;
using Swashbuckle.AspNetCore.SwaggerGen;

namespace RestFulServer.Web.Filters
{
    public class SwaggerOperationNameFilter : IOperationFilter
    {
        public void Apply(Operation operation, OperationFilterContext context)
        {
            operation.OperationId = (context.ApiDescription.ActionDescriptor as ControllerActionDescriptor)?.ControllerName + "_" + ((Microsoft.AspNetCore.Mvc.Controllers.ControllerActionDescriptor)context.ApiDescription.ActionDescriptor).ActionName.Replace("Async", string.Empty);
        }
    }
}
