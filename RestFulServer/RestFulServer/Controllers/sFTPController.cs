﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;
using FileServer.Services;
using FileServer.Services.sFTP;

using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace RestFulServer.Controllers
{
    [Route("api/[controller]")]
    public class sFTPController : ControllerBase
    {
		private readonly IsFTP _sFtpService;
		public sFTPController(IsFTP sFtpService)//, IPlcCrudService plcCrudService) : base(plcCrudService)
		{
			_sFtpService = sFtpService;
		}

		[HttpGet("Files")]
		public List<string> GetFilesOnMSC3()
		{
			return _sFtpService.GetFilesOnRemote(); ;
		}
        [HttpGet("CopyFiles")]
        public List<string> CopyFilesOnMSC3()
        {
            return _sFtpService.CopyAllFromRemote();
        }
        [HttpGet("DeleteOldFiles")]
        public List<string> DeleteOldFilesOnMSC3()
        {
            return _sFtpService.DeleteOld();
        }

    }
}
