﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;
using FileServer.Services;
using FileServer.Services.Files;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace RestFulServer.Controllers
{
    [Route("api/[controller]")]
    public class FileController : ControllerBase
    {
		private readonly IFileServices _fileService;
		public FileController(IFileServices fileService)//, IPlcCrudService plcCrudService) : base(plcCrudService)
		{
			_fileService = fileService;
		}

		// GET: api/Test
		[HttpGet]
        public IEnumerable<string> Get()
        {
			return _fileService.FileNames(); ;
        }

		[HttpPut("csv")]
		public void FilePut([FromQuery]string FileName, [FromBody] string Content)
		{
			if (Content != null)
			{
				_fileService.RecieveFile(FileName,Content);
			}
		}
		[HttpPut("FileNames")]
		public void FileNames([FromBody] List<string> Files)
		{
			_fileService.RecieveFileNames(Files);
		}
		[HttpGet("FileNameRequest")]
		public string GetFileNameRequest()
		{
			return _fileService.FileNameRequest(); ;
		}
		[HttpGet("FilesToDelete")]
		public List<string> GetFilesToDelete()
		{
			return _fileService.FilesReadyForDelete(); ;
		}
		[HttpGet("Settings")]
		public FileSettings GetFileSettings()
		{
			return _fileService.GetSettings();
		}
		[HttpPost("FileFolder")]
		public void GetFileSettings([FromBody] FileFolders filter)
		{
			_fileService.PostFileFolder(filter);
		}
		[HttpGet("Version")]
		public string GetVersion()
		{
			string VersionInfo = "";

			VersionInfo += Assembly.GetExecutingAssembly().GetName().Name.ToString();
			VersionInfo += "\n";
			VersionInfo += Assembly.GetExecutingAssembly().GetName().Version.ToString();
			VersionInfo += "\n";
			VersionInfo += new DateTime(Builtin.CompileTime, DateTimeKind.Utc).ToString();

			return VersionInfo;

		}
	}
}
