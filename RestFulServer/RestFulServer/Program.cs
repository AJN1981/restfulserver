﻿using System.Diagnostics;
using System.IO;
using Autofac.Extensions.DependencyInjection;
using Microsoft.AspNetCore;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Hosting.WindowsServices;
using System.Linq;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using System.Net;
namespace RestFulServer
{
	public class Program
	{
		public static void Main(string[] args)
		{
			bool isService = !(Debugger.IsAttached || args.Contains("--console"));

			var pathToContentRoot = Directory.GetCurrentDirectory();
			if (isService)
			{
				var pathToExe = Process.GetCurrentProcess().MainModule.FileName;
				pathToContentRoot = Path.GetDirectoryName(pathToExe);
			}

			var host = WebHost.CreateDefaultBuilder()
				.UseKestrel(options =>
				{
					options.Listen(IPAddress.Any, 8079);
				})
				.UseContentRoot(pathToContentRoot)
				.ConfigureServices(services => services.AddAutofac())
				.ConfigureAppConfiguration((hostingContext, config) =>
				{
					var env = hostingContext.HostingEnvironment;
					config
						.AddJsonFile("appsettings.json", optional: true, reloadOnChange: true)
						.AddJsonFile($"appsettings.{env.EnvironmentName}.json", optional: true, reloadOnChange: true)
						.AddEnvironmentVariables();
				})
				//.UseIISIntegration()
				.ConfigureLogging((hostingContext, logging) =>
				{
					logging.AddConfiguration(hostingContext.Configuration.GetSection("Logging"));
					logging.AddEventLog();
					logging.AddConsole();
					logging.AddDebug();
				})
				.UseStartup<Startup>()
				.Build();

			if (isService)
			{
				host.RunAsService();
			}
			else
			{
				host.Run();
			}
		}
	}
}
