﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace FileServer.Services.Files
{
	public class FileSettings
	{
		public string dir { get; set; }

		public virtual void Load(string filename)
		{
			if (File.Exists(filename))
				using (StreamReader s = new StreamReader(filename))
					JsonConvert.PopulateObject(s.ReadToEnd(), this);
		}
		public virtual void Save(string filename)
		{
			Directory.CreateDirectory(Path.GetDirectoryName(filename));
			using (StreamWriter w = new StreamWriter(filename))
				w.WriteLine(JsonConvert.SerializeObject(this, Newtonsoft.Json.Formatting.Indented));
		}
	}
	
}
