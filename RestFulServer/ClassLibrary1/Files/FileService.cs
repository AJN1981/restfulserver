﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;

namespace FileServer.Services.Files
{
	public class FileService : IFileServices
	{
		//private static string dir = @"C:\TestFolder\";
		private static int count = 1;
		private static int DaysKeep = 0; //Negative number
		private static List<string> _filesNamesOnMSC;
		private NextCloud.NextCloud _cloud = new NextCloud.NextCloud();

		private static FileSettings _settings;
		public  FileSettings Settings
		{
			get
			{
				if (_settings == null)
				{
					string dataPath = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "Settings");
					Directory.CreateDirectory(dataPath);
					string filename = Path.Combine(dataPath, "FileSettings.json");
					_settings = new FileSettings();
					_settings.Load(filename);
				}
				return _settings;
			}
		}
		public List<string> FileNames()
		{
			if (_filesNamesOnMSC == null)
			{
				_filesNamesOnMSC = new List<string>();
				_filesNamesOnMSC.Add("Empty");
			}
			//var tmp =_cloud.postFile();
			return _filesNamesOnMSC;
		
		}

		public void WriteFileNames(List<string> fileNames)
		{
			if (fileNames != null)
			{
				_filesNamesOnMSC = fileNames;
			}
		}

		public void RecieveFile(string FileName, string data)
		{
			var FilePath = getFilePath(FileName);
			if (!File.Exists(FilePath))
			{
				using (StreamWriter file = new StreamWriter(FilePath))
				{
					file.Write(data);
				}
			}

			_cloud.postFile();
		}
		private string getFilePath(string FileName)
		{
			string _dir = Settings.dir;
			string fileType = Path.GetExtension(FileName);
			switch (fileType)
			{
				case ".csv":
					_dir += @"Test\";
					break;
				default:
					_dir += @"Settings\";
					break;

			}
			return _dir + FileName;
		}


		public void RecieveFileNames(List<string> FileNames)
		{
			if (FileNames != null)
			{
				_filesNamesOnMSC = FileNames;
			}
		}

		public string FileNameRequest()
		{
			if (_filesNamesOnMSC != null)
			{
				foreach (string filename in _filesNamesOnMSC)
				{

					if (!File.Exists(getFilePath(filename)))
					{ return filename; }
				}
			}
			return null;
		}
		public List<string> FilesReadyForDelete()
		{
			List<string> Files = new List<string>();
			if (_filesNamesOnMSC != null)
			{
				foreach (string filename in _filesNamesOnMSC)
				{
					var FilePath = getFilePath(filename);
					if (File.Exists(FilePath))
					{
						if (File.GetCreationTime(FilePath) < DateTime.Now.AddDays(DaysKeep))
						{
							Files.Add(filename);
						}
					}
				}
			}
			return Files;
		}


	}
}
