﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using Newtonsoft.Json;
using NextcloudApi;

namespace FileServer.Services.NextCloud
{
	public class NextCloud : INextCloud
	{
		private static int i = 0;
		static Settings _settings;

		public static Settings Settings
		{
			get
			{
				if (_settings == null)
				{
					string dataPath = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "Settings");
					Directory.CreateDirectory(dataPath);
					string filename = Path.Combine(dataPath, "NextCloudSettings.json");
					_settings = new Settings();
					_settings.Load(filename);
					List<string> errors = _settings.Validate();
					if (errors.Count > 0)
						;//throw new ApplicationException(string.Join("\r\n", errors));
				}
				return _settings;
			}
		}
		static Api _cloud;

		public static Api Cloud
		{
			get
			{
				if (_cloud == null)
				{
					//var tmp = JsonConvert.DeserializeObject<Settings>(JsonConvert.SerializeObject(Settings));
					_cloud = new Api(Settings);
				}
				return _cloud;
			}
		}


		public int postFile()
		{
			var t = Cloud.LoginAsync();
			t.Wait();
			//var t1 = CloudFolder.List(Cloud,"27%20-%20Moog%20%26%20Micron&fileid=117330",Settings,2);
			var t1 = CloudFolder.List(Cloud,Settings.Username);
			t1.Wait();

			return i++;
		}
	}
}
